//
//  RepositoryStore.swift
//  DesafioiOS
//
//  Created by José Guilherme de Lima Freitas on 19/12/2017.
//  Copyright © 2017 JG. All rights reserved.
//

import UIKit

class RepositoryStore: BaseStore {

    func loadRepositories(fromPage page: Int, response: @escaping (([Repository]) -> Void)){
        
        request(option: .repositories(page: page)) { (data) in
            let repList = self.mapToRepositories(fromData: data)
            guard let list = repList else {return}
            response(list.repositories)
        }
    }
    
    private func mapToRepositories(fromData data: Data) -> RepositoryList?{
        
        do{
            let decoder = JSONDecoder()
            let repList = try decoder.decode(RepositoryList.self, from: data)
            return repList
        }catch let error{
            print("Error on decoding repositories: \(error)")
            return nil
        }
    }
}
