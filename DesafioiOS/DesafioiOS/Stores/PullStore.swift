//
//  PullStore.swift
//  DesafioiOS
//
//  Created by José Guilherme de Lima Freitas on 19/12/2017.
//  Copyright © 2017 JG. All rights reserved.
//

import UIKit

class PullStore: BaseStore {

    func loadPullRequests(author: String, repository: String, response: @escaping (([PullRequest]) -> Void)){

        request(option: .pullRequests(author: author, repository: repository)) { (data) in
            let pullList = self.mapToPullRequests(fromData: data)
            guard let list = pullList else {return}
            response(list)
        }
    }
    
    private func mapToPullRequests(fromData data: Data) -> [PullRequest]?{
        
        do{
            let decoder = JSONDecoder()
            return try decoder.decode([PullRequest].self, from: data)
        }catch let error{
            print("Error on decoding pull requests: \(error)")
            return nil
        }
    }
}
