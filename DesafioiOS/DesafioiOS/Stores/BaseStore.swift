//
//  BaseStore.swift
//  DesafioiOS
//
//  Created by José Guilherme de Lima Freitas on 19/12/2017.
//  Copyright © 2017 JG. All rights reserved.
//

import UIKit
import Alamofire

class BaseStore: NSObject {

    enum URLStrings{
        case repositories(page: Int)
        case pullRequests(author: String, repository: String)
        
        func description() -> String{
            switch self {
            case .repositories(let page):
                return "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)"
            case .pullRequests(let author, let repository):
                return "https://api.github.com/repos/\(author)/\(repository)/pulls"
            }
        }
    }
    
    func request(option: URLStrings, callback: @escaping ((Data) -> Void)){
        guard let url = URL(string: option.description()) else {return}
        
        Alamofire.request(url).responseData { (response) in
            switch response.result{
            case .success(let data):
                callback(data)
            case .failure(let error):
                print("Error on request \(error)")
            }
        }
    }
}
