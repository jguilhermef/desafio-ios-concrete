//
//  RepositoryDataSource.swift
//  DesafioiOS
//
//  Created by José Guilherme de Lima Freitas on 20/12/2017.
//  Copyright © 2017 JG. All rights reserved.
//

import UIKit

class RepositoryDataSource: NSObject, UITableViewDataSource{
    
    private var reps: [Repository] = []
    
    required init(reps: [Repository]){
        self.reps = reps
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reps.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryTableViewCell", for: indexPath) as! RepositoryTableViewCell
        
        cell.fill(repository: reps[indexPath.row])
    
        return cell
    }
    
    func update(newReps: [Repository]){
        for rep in newReps{
            reps.append(rep)
        }
    }
    
    func getRepository(fromIndex index: Int) -> Repository{
        return reps[index]
    }
    
    func repositoriesCount() -> Int{
        return reps.count
    }
}
