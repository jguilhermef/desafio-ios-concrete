//
//  PullRequestDataSource.swift
//  DesafioiOS
//
//  Created by José Guilherme de Lima Freitas on 20/12/2017.
//  Copyright © 2017 JG. All rights reserved.
//

import UIKit

class PullRequestDataSource: NSObject, UITableViewDataSource{

    private var prequests: [PullRequest] = []
    
    required init(pullRequests: [PullRequest]){
        self.prequests = pullRequests
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PullRequestTableViewCell", for: indexPath) as! PullRequestTableViewCell
        
        cell.fill(pullRequest: prequests[indexPath.row])
        
        return cell
    }
    
    func getURL(index: Int) -> URL{
        return prequests[index].webPage
    }
}
