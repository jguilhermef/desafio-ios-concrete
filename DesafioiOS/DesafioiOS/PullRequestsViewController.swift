//
//  PullRequestsViewController.swift
//  DesafioiOS
//
//  Created by José Guilherme de Lima Freitas on 20/12/2017.
//  Copyright © 2017 JG. All rights reserved.
//

import UIKit

class PullRequestsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var     author: String = ""
    var repository: String = ""
    var dataSource: PullRequestDataSource?
    
    var activity = AnimationView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = repository
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activity)
        
        let pullStore = PullStore()
        pullStore.loadPullRequests(author: self.author, repository: self.repository) { (pullList) in
            if pullList.count != 0 {
                self.setupTableView(prs: pullList)
            }else{
                self.setupMessage()
            }
            self.activity.stopAnimating()
        }
    }
    
    func setupTableView(prs: [PullRequest]){
        dataSource = PullRequestDataSource(pullRequests: prs)
        self.tableView.delegate = self
        self.tableView.dataSource = dataSource
        self.tableView.reloadData()
        UIView.animate(withDuration: 1) {self.tableView.alpha = 1}
    }
    
    func setupMessage(){
        let message = UIAlertController(title: "Resultado", message: "Este repositório não possui pull requests!", preferredStyle: .alert)
        message.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(message, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension PullRequestsViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let url = dataSource?.getURL(index: indexPath.row) else {return}
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
