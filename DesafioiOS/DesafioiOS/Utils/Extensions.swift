//
//  Extensions.swift
//  DesafioiOS
//
//  Created by José Guilherme de Lima Freitas on 20/12/2017.
//  Copyright © 2017 JG. All rights reserved.
//

import UIKit

extension UIImageView{
    
    func roundCorner(){
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}

func convertDate(dt: String) -> String {
    let dateFormatter = DateFormatter()
    let tempLocale = dateFormatter.locale
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    guard let date = dateFormatter.date(from: dt) else { return "" }
    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
    dateFormatter.locale = tempLocale
    let dateString = dateFormatter.string(from: date)
    return dateString
}
