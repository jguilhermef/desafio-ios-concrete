//
//  ViewController.swift
//  DesafioiOS
//
//  Created by José Guilherme de Lima Freitas on 19/12/2017.
//  Copyright © 2017 JG. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var dataSource: RepositoryDataSource?
    var  pageIndex: Int = 1
    
    var activity = AnimationView()
    let repStore = RepositoryStore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activity)
       
        self.repStore.loadRepositories(fromPage: pageIndex) { (reps) in
            self.setupTableView(reps: reps)
            self.activity.stopAnimating()
        }
    }
    
    func setupTableView(reps: [Repository]){
        dataSource = RepositoryDataSource(reps: reps)
        self.tableView.delegate = self
        self.tableView.dataSource = dataSource
        self.tableView.reloadData()
        UIView.animate(withDuration: 1) {self.tableView.alpha = 1}
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let repsCount = dataSource?.repositoriesCount() else {return}
    
        if indexPath.row == repsCount - 1 && (pageIndex * 30) < 1000{
            self.pageIndex += 1
            self.activity.startAnimating()
            
            repStore.loadRepositories(fromPage: pageIndex, response: { (reps) in
                self.dataSource?.update(newReps: reps)
                self.tableView.reloadData()
                self.activity.stopAnimating()
            })
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let rep = dataSource?.getRepository(fromIndex: indexPath.row) else {return}
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "PullRequestsViewController") as? PullRequestsViewController else {return}
        vc.author = rep.author.name
        vc.repository = rep.name
        self.show(vc, sender: self)
    }
}
