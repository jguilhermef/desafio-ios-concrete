//
//  PullRequestTableViewCell.swift
//  DesafioiOS
//
//  Created by José Guilherme de Lima Freitas on 20/12/2017.
//  Copyright © 2017 JG. All rights reserved.
//

import UIKit
import SDWebImage

class PullRequestTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var username: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.userPhoto.roundCorner()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fill(pullRequest pr: PullRequest){
        self.title.text = pr.title
        self.body.text = pr.body
        self.date.text = convertDate(dt: pr.date)
        self.username.text = pr.author.name
        self.userPhoto.sd_setShowActivityIndicatorView(true)
        self.userPhoto.sd_setIndicatorStyle(.gray)
        self.userPhoto.sd_setImage(with: pr.author.photo, completed: nil)
    }

}
