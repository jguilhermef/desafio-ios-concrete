//
//  RepositoryTableViewCell.swift
//  DesafioiOS
//
//  Created by José Guilherme de Lima Freitas on 20/12/2017.
//  Copyright © 2017 JG. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var forks: UILabel!
    @IBOutlet weak var stars: UILabel!
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var username: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.userPhoto.roundCorner()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fill(repository rep: Repository){
        self.name.text = rep.name
        self.desc.text = rep.description
        self.forks.text = String(rep.forks)
        self.stars.text = String(rep.stars)
        self.username.text = rep.author.name
        self.userPhoto.sd_setShowActivityIndicatorView(true)
        self.userPhoto.sd_setIndicatorStyle(.gray)
        self.userPhoto.sd_setImage(with: rep.author.photo, completed: nil)
    }
}
