//
//  AnimationView.swift
//  DesafioiOS
//
//  Created by José Guilherme de Lima Freitas on 20/12/2017.
//  Copyright © 2017 JG. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class AnimationView: UIView {

    var activity: NVActivityIndicatorView
    
    init() {
        let frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        self.activity = NVActivityIndicatorView(frame: frame, type: .ballScaleRippleMultiple, color: .gray, padding: nil)
        
        super.init(frame: frame)
        self.addSubview(activity)
        self.startAnimating()
    }
    
    func startAnimating(){
        UIView.animate(withDuration: 1) {
            self.activity.alpha = 1
            self.activity.startAnimating()
        }
    }
    
    func stopAnimating(){
        UIView.animate(withDuration: 1, animations: {
            self.activity.alpha = 0
        }) { (anim) in
            self.activity.stopAnimating()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
