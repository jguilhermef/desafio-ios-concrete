//
//  Author.swift
//  DesafioiOS
//
//  Created by José Guilherme de Lima Freitas on 19/12/2017.
//  Copyright © 2017 JG. All rights reserved.
//

import Foundation

struct Author: Codable{
    let   name: String
    let  photo: URL
    
    enum CodingKeys: String, CodingKey {
        case  name = "login"
        case photo = "avatar_url"
    }
}
