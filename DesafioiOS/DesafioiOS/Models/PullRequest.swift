//
//  PullRequest.swift
//  DesafioiOS
//
//  Created by José Guilherme de Lima Freitas on 19/12/2017.
//  Copyright © 2017 JG. All rights reserved.
//

import Foundation

struct PullRequest: Codable{
    let   title: String
    let    date: String
    let    body: String?
    let webPage: URL
    let  author: Author
    
    enum CodingKeys: String, CodingKey {
        case  author = "user"
        case  date = "created_at"
        case webPage = "html_url"
        case  title
        case  body
    }
}
