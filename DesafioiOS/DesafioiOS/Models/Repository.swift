//
//  Repository.swift
//  DesafioiOS
//
//  Created by José Guilherme de Lima Freitas on 19/12/2017.
//  Copyright © 2017 JG. All rights reserved.
//

import Foundation

struct Repository: Codable{
    let        name: String
    let description: String?
    let      author: Author
    let       stars: Int
    let       forks: Int
    
    enum CodingKeys: String, CodingKey {
        case description
        case      author = "owner"
        case       stars = "stargazers_count"
        case       forks
        case        name
    }
}

struct RepositoryList: Codable {
    let repositories: [Repository]
    
    enum CodingKeys: String, CodingKey {
        case repositories = "items"
    }
}


