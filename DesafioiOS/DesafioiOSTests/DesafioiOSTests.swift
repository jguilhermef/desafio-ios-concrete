//
//  DesafioiOSTests.swift
//  DesafioiOSTests
//
//  Created by José Guilherme de Lima Freitas on 21/12/2017.
//  Copyright © 2017 JG. All rights reserved.
//

import XCTest

@testable import DesafioiOS

class DesafioiOSTests: XCTestCase {
    
    var   vc: ViewController!
    var prvc: PullRequestsViewController!
    
    override func setUp() {
        super.setUp()
        vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewController") as! ViewController
        _ = vc.view
        
        prvc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PullRequestsViewController") as! PullRequestsViewController
        _ = prvc.view
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testInitialPageIndexValue(){
        XCTAssertEqual(vc.pageIndex, 1)
    }
    
    func testNumberOfRowsInSectionRepositoriesTableView(){
        let rep = Repository(name: "Desafio iOS", description: "Unitário", author: Author(name: "JG", photo: URL(string: "http")!), stars: 10, forks: 10)
        let ds = RepositoryDataSource(reps: [rep, rep])
        vc.tableView.dataSource = ds
        vc.tableView.reloadData()
        
        XCTAssertEqual(vc.tableView.numberOfRows(inSection: 0), ds.repositoriesCount())
    }
    
    func testNumberOfRowsInSectionPullRequestsTableView(){
        let pr = PullRequest(title: "Desafio", date: "2017-12-01T19:48:55Z", body: "Organizando", webPage: URL(string: "http")!, author: Author(name: "JG", photo: URL(string: "http")!))
        let ds = PullRequestDataSource(pullRequests: [pr, pr])
        prvc.tableView.dataSource = ds
        prvc.tableView.reloadData()
        
        XCTAssertEqual(prvc.tableView.numberOfRows(inSection: 0), 2)
    }
    
    func testRowHeightOfPullRequestsTableView(){
        XCTAssertEqual(prvc.tableView.rowHeight, 110)
    }
    
    func testRowHeightOfRepositoryTableView(){
        XCTAssertEqual(vc.tableView.rowHeight, 124)
    }
    
    func testRepositoriesViewControllerTitle(){
        XCTAssertEqual(vc.navigationItem.title, "GitHub JavaPop")
    }
    
}
